﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XmlConfigurationEditor
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void Save()
        {
            // MainXmlManager.Save()
        }

        private void QuitToolStripButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void NewToolStripButton_Click(object sender, EventArgs e)
        {
            MainXmlManager.ConfigDocument = new XMLConfig.XConfigDocument();
            MainXmlManager.ConfigurationFile = null;
        }

        private void LoadToolStripButton_Click(object sender, EventArgs e)
        {
            if (OpenConfigFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                MainXmlManager.ConfigurationFile = OpenConfigFileDialog.FileName;
            }
        }

        private void SaveToolStripSplitButton_ButtonClick(object sender, EventArgs e)
        {
            MainXmlManager.Save();
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainXmlManager.SaveAs();
        }
    }
}
