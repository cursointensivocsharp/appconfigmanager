﻿namespace XmlConfigurationEditor
{
    partial class MainForm
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MainTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.MainToolStrip = new System.Windows.Forms.ToolStrip();
            this.NewToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.LoadToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.SaveToolStripSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this.SaveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.QuitToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.MainStatusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.OpenConfigFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.MainXmlManager = new UserControls.Windows.Forms.XmlConfigManagerControl();
            this.MainTableLayoutPanel.SuspendLayout();
            this.MainToolStrip.SuspendLayout();
            this.MainStatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainTableLayoutPanel
            // 
            resources.ApplyResources(this.MainTableLayoutPanel, "MainTableLayoutPanel");
            this.MainTableLayoutPanel.Controls.Add(this.MainXmlManager, 0, 1);
            this.MainTableLayoutPanel.Controls.Add(this.MainToolStrip, 0, 0);
            this.MainTableLayoutPanel.Controls.Add(this.MainStatusStrip, 0, 2);
            this.MainTableLayoutPanel.Name = "MainTableLayoutPanel";
            // 
            // MainToolStrip
            // 
            this.MainToolStrip.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.MainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewToolStripButton,
            this.LoadToolStripButton,
            this.SaveToolStripSplitButton,
            this.QuitToolStripButton});
            resources.ApplyResources(this.MainToolStrip, "MainToolStrip");
            this.MainToolStrip.Name = "MainToolStrip";
            // 
            // NewToolStripButton
            // 
            resources.ApplyResources(this.NewToolStripButton, "NewToolStripButton");
            this.NewToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.NewToolStripButton.Image = global::XmlConfigurationEditor.Properties.Resources.file_code;
            this.NewToolStripButton.Name = "NewToolStripButton";
            this.NewToolStripButton.Click += new System.EventHandler(this.NewToolStripButton_Click);
            // 
            // LoadToolStripButton
            // 
            this.LoadToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.LoadToolStripButton.Image = global::XmlConfigurationEditor.Properties.Resources.folder_open;
            resources.ApplyResources(this.LoadToolStripButton, "LoadToolStripButton");
            this.LoadToolStripButton.Name = "LoadToolStripButton";
            this.LoadToolStripButton.Click += new System.EventHandler(this.LoadToolStripButton_Click);
            // 
            // SaveToolStripSplitButton
            // 
            this.SaveToolStripSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.SaveToolStripSplitButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SaveAsToolStripMenuItem});
            this.SaveToolStripSplitButton.Image = global::XmlConfigurationEditor.Properties.Resources.save;
            resources.ApplyResources(this.SaveToolStripSplitButton, "SaveToolStripSplitButton");
            this.SaveToolStripSplitButton.Name = "SaveToolStripSplitButton";
            this.SaveToolStripSplitButton.ButtonClick += new System.EventHandler(this.SaveToolStripSplitButton_ButtonClick);
            // 
            // SaveAsToolStripMenuItem
            // 
            this.SaveAsToolStripMenuItem.Name = "SaveAsToolStripMenuItem";
            resources.ApplyResources(this.SaveAsToolStripMenuItem, "SaveAsToolStripMenuItem");
            this.SaveAsToolStripMenuItem.Click += new System.EventHandler(this.SaveAsToolStripMenuItem_Click);
            // 
            // QuitToolStripButton
            // 
            this.QuitToolStripButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.QuitToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.QuitToolStripButton.Image = global::XmlConfigurationEditor.Properties.Resources.window_close;
            resources.ApplyResources(this.QuitToolStripButton, "QuitToolStripButton");
            this.QuitToolStripButton.Name = "QuitToolStripButton";
            this.QuitToolStripButton.Click += new System.EventHandler(this.QuitToolStripButton_Click);
            // 
            // MainStatusStrip
            // 
            this.MainStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            resources.ApplyResources(this.MainStatusStrip, "MainStatusStrip");
            this.MainStatusStrip.Name = "MainStatusStrip";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            resources.ApplyResources(this.toolStripStatusLabel1, "toolStripStatusLabel1");
            // 
            // OpenConfigFileDialog
            // 
            resources.ApplyResources(this.OpenConfigFileDialog, "OpenConfigFileDialog");
            // 
            // MainXmlManager
            // 
            resources.ApplyResources(this.MainXmlManager, "MainXmlManager");
            this.MainXmlManager.Name = "MainXmlManager";
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MainTableLayoutPanel);
            this.Name = "MainForm";
            this.MainTableLayoutPanel.ResumeLayout(false);
            this.MainTableLayoutPanel.PerformLayout();
            this.MainToolStrip.ResumeLayout(false);
            this.MainToolStrip.PerformLayout();
            this.MainStatusStrip.ResumeLayout(false);
            this.MainStatusStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel MainTableLayoutPanel;
        private UserControls.Windows.Forms.XmlConfigManagerControl MainXmlManager;
        private System.Windows.Forms.ToolStrip MainToolStrip;
        private System.Windows.Forms.ToolStripButton NewToolStripButton;
        private System.Windows.Forms.ToolStripButton LoadToolStripButton;
        private System.Windows.Forms.StatusStrip MainStatusStrip;
        private System.Windows.Forms.ToolStripSplitButton SaveToolStripSplitButton;
        private System.Windows.Forms.ToolStripMenuItem SaveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton QuitToolStripButton;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.OpenFileDialog OpenConfigFileDialog;
    }
}

