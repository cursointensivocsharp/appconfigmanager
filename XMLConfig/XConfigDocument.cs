﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Linq;

namespace XMLConfig
{
    /// <summary>
    /// An XML Configuration file.
    /// </summary>
    public class XConfigDocument : XDocument
    {
        #region Constructors

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="declaration">XML declaration.</param>
        public XConfigDocument(XDeclaration declaration): base(declaration, new XElement(ROOT_ELEMENT_NAME))
        {
        }
        
        /// <summary>
        /// Constructor.
        /// </summary>
        public XConfigDocument() : this(new XDeclaration("1.0", "UTF-8", "no"))
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="document">XDocument to be validated and copied</param>
        public XConfigDocument(XDocument document): base(document)
        {
            if (Root.Name != ROOT_ELEMENT_NAME)
            {
                throw new Exceptions.InvalidXConfigDocumentException(XConfigDocument_Messages.InvalidXmlDocument);
            }
        }

        #endregion Constructors


        #region Properties 

        public string DocumentHash
        {
            get
            {
                var xml = $"{Declaration}{ToString(SaveOptions.DisableFormatting | SaveOptions.None)}";
                byte[] hash;

                using (var hasher = new SHA1Managed())
                {
                    hash = hasher.ComputeHash(Encoding.UTF8.GetBytes(xml));
                }

                return string.Join("", hash.Select(b => b.ToString("X2")).ToArray());
            }
        }

        #endregion Properties


        #region Constants

        /// <summary>
        /// The name of the Root element of an XML configuration file.
        /// </summary>
        public const string ROOT_ELEMENT_NAME = "configuration";

        #endregion Constants
    }
}
