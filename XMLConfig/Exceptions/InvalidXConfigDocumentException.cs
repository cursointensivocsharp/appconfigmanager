﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLConfig.Exceptions
{
    public class InvalidXConfigDocumentException : Exception
    {
        public InvalidXConfigDocumentException(string message, Exception innerException = null) : base(message, innerException)
        {
        }
    }
}
