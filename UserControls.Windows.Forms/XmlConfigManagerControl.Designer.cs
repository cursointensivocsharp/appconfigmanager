﻿namespace UserControls.Windows.Forms
{
    partial class XmlConfigManagerControl
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XmlConfigManagerControl));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.MainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.NodesListTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.NodesTreeView = new System.Windows.Forms.TreeView();
            this.AddNodeButton = new System.Windows.Forms.Button();
            this.RemoveSelectedNodeButton = new System.Windows.Forms.Button();
            this.EditSelectedNodeButton = new System.Windows.Forms.Button();
            this.SelectedNodeDetailsSplitContainer = new System.Windows.Forms.SplitContainer();
            this.SelectedNodeContentGroupBox = new System.Windows.Forms.GroupBox();
            this.SelectedNodeContentTextBox = new System.Windows.Forms.TextBox();
            this.SelectedNodeAttributesGroupBox = new System.Windows.Forms.GroupBox();
            this.SelectedNodeDetailsTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.SelectedNodeAttributesDataGridView = new System.Windows.Forms.DataGridView();
            this.NameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValueColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttributesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.AddAttributeButton = new System.Windows.Forms.Button();
            this.RemoveAttributesButton = new System.Windows.Forms.Button();
            this.EditAttributeButton = new System.Windows.Forms.Button();
            this.MainToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SaveDocumentDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.MainSplitContainer)).BeginInit();
            this.MainSplitContainer.Panel1.SuspendLayout();
            this.MainSplitContainer.Panel2.SuspendLayout();
            this.MainSplitContainer.SuspendLayout();
            this.NodesListTableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedNodeDetailsSplitContainer)).BeginInit();
            this.SelectedNodeDetailsSplitContainer.Panel1.SuspendLayout();
            this.SelectedNodeDetailsSplitContainer.Panel2.SuspendLayout();
            this.SelectedNodeDetailsSplitContainer.SuspendLayout();
            this.SelectedNodeContentGroupBox.SuspendLayout();
            this.SelectedNodeAttributesGroupBox.SuspendLayout();
            this.SelectedNodeDetailsTableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedNodeAttributesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AttributesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // MainSplitContainer
            // 
            resources.ApplyResources(this.MainSplitContainer, "MainSplitContainer");
            this.MainSplitContainer.Name = "MainSplitContainer";
            // 
            // MainSplitContainer.Panel1
            // 
            resources.ApplyResources(this.MainSplitContainer.Panel1, "MainSplitContainer.Panel1");
            this.MainSplitContainer.Panel1.Controls.Add(this.NodesListTableLayoutPanel);
            this.MainToolTip.SetToolTip(this.MainSplitContainer.Panel1, resources.GetString("MainSplitContainer.Panel1.ToolTip"));
            // 
            // MainSplitContainer.Panel2
            // 
            resources.ApplyResources(this.MainSplitContainer.Panel2, "MainSplitContainer.Panel2");
            this.MainSplitContainer.Panel2.Controls.Add(this.SelectedNodeDetailsSplitContainer);
            this.MainToolTip.SetToolTip(this.MainSplitContainer.Panel2, resources.GetString("MainSplitContainer.Panel2.ToolTip"));
            this.MainToolTip.SetToolTip(this.MainSplitContainer, resources.GetString("MainSplitContainer.ToolTip"));
            // 
            // NodesListTableLayoutPanel
            // 
            resources.ApplyResources(this.NodesListTableLayoutPanel, "NodesListTableLayoutPanel");
            this.NodesListTableLayoutPanel.Controls.Add(this.NodesTreeView, 0, 0);
            this.NodesListTableLayoutPanel.Controls.Add(this.AddNodeButton, 0, 1);
            this.NodesListTableLayoutPanel.Controls.Add(this.RemoveSelectedNodeButton, 1, 1);
            this.NodesListTableLayoutPanel.Controls.Add(this.EditSelectedNodeButton, 2, 1);
            this.NodesListTableLayoutPanel.Name = "NodesListTableLayoutPanel";
            this.MainToolTip.SetToolTip(this.NodesListTableLayoutPanel, resources.GetString("NodesListTableLayoutPanel.ToolTip"));
            // 
            // NodesTreeView
            // 
            resources.ApplyResources(this.NodesTreeView, "NodesTreeView");
            this.NodesListTableLayoutPanel.SetColumnSpan(this.NodesTreeView, 3);
            this.NodesTreeView.HideSelection = false;
            this.NodesTreeView.LabelEdit = true;
            this.NodesTreeView.Name = "NodesTreeView";
            this.MainToolTip.SetToolTip(this.NodesTreeView, resources.GetString("NodesTreeView.ToolTip"));
            this.NodesTreeView.BeforeLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.NodesTreeView_BeforeLabelEdit);
            this.NodesTreeView.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.NodesTreeView_AfterLabelEdit);
            this.NodesTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.NodesTreeView_AfterSelect);
            // 
            // AddNodeButton
            // 
            resources.ApplyResources(this.AddNodeButton, "AddNodeButton");
            this.AddNodeButton.Image = global::UserControls.Windows.Forms.Properties.Resources.plus;
            this.AddNodeButton.Name = "AddNodeButton";
            this.MainToolTip.SetToolTip(this.AddNodeButton, resources.GetString("AddNodeButton.ToolTip"));
            this.AddNodeButton.UseVisualStyleBackColor = true;
            this.AddNodeButton.Click += new System.EventHandler(this.AddNodeButton_Click);
            // 
            // RemoveSelectedNodeButton
            // 
            resources.ApplyResources(this.RemoveSelectedNodeButton, "RemoveSelectedNodeButton");
            this.RemoveSelectedNodeButton.Image = global::UserControls.Windows.Forms.Properties.Resources.times;
            this.RemoveSelectedNodeButton.Name = "RemoveSelectedNodeButton";
            this.MainToolTip.SetToolTip(this.RemoveSelectedNodeButton, resources.GetString("RemoveSelectedNodeButton.ToolTip"));
            this.RemoveSelectedNodeButton.UseVisualStyleBackColor = true;
            this.RemoveSelectedNodeButton.Click += new System.EventHandler(this.RemoveNodeButton_Click);
            // 
            // EditSelectedNodeButton
            // 
            resources.ApplyResources(this.EditSelectedNodeButton, "EditSelectedNodeButton");
            this.EditSelectedNodeButton.Image = global::UserControls.Windows.Forms.Properties.Resources.edit;
            this.EditSelectedNodeButton.Name = "EditSelectedNodeButton";
            this.MainToolTip.SetToolTip(this.EditSelectedNodeButton, resources.GetString("EditSelectedNodeButton.ToolTip"));
            this.EditSelectedNodeButton.UseVisualStyleBackColor = true;
            this.EditSelectedNodeButton.Click += new System.EventHandler(this.EditSelectedNodeButton_Click);
            // 
            // SelectedNodeDetailsSplitContainer
            // 
            resources.ApplyResources(this.SelectedNodeDetailsSplitContainer, "SelectedNodeDetailsSplitContainer");
            this.SelectedNodeDetailsSplitContainer.Name = "SelectedNodeDetailsSplitContainer";
            // 
            // SelectedNodeDetailsSplitContainer.Panel1
            // 
            resources.ApplyResources(this.SelectedNodeDetailsSplitContainer.Panel1, "SelectedNodeDetailsSplitContainer.Panel1");
            this.SelectedNodeDetailsSplitContainer.Panel1.Controls.Add(this.SelectedNodeContentGroupBox);
            this.MainToolTip.SetToolTip(this.SelectedNodeDetailsSplitContainer.Panel1, resources.GetString("SelectedNodeDetailsSplitContainer.Panel1.ToolTip"));
            // 
            // SelectedNodeDetailsSplitContainer.Panel2
            // 
            resources.ApplyResources(this.SelectedNodeDetailsSplitContainer.Panel2, "SelectedNodeDetailsSplitContainer.Panel2");
            this.SelectedNodeDetailsSplitContainer.Panel2.Controls.Add(this.SelectedNodeAttributesGroupBox);
            this.MainToolTip.SetToolTip(this.SelectedNodeDetailsSplitContainer.Panel2, resources.GetString("SelectedNodeDetailsSplitContainer.Panel2.ToolTip"));
            this.MainToolTip.SetToolTip(this.SelectedNodeDetailsSplitContainer, resources.GetString("SelectedNodeDetailsSplitContainer.ToolTip"));
            // 
            // SelectedNodeContentGroupBox
            // 
            resources.ApplyResources(this.SelectedNodeContentGroupBox, "SelectedNodeContentGroupBox");
            this.SelectedNodeContentGroupBox.Controls.Add(this.SelectedNodeContentTextBox);
            this.SelectedNodeContentGroupBox.Name = "SelectedNodeContentGroupBox";
            this.SelectedNodeContentGroupBox.TabStop = false;
            this.MainToolTip.SetToolTip(this.SelectedNodeContentGroupBox, resources.GetString("SelectedNodeContentGroupBox.ToolTip"));
            // 
            // SelectedNodeContentTextBox
            // 
            resources.ApplyResources(this.SelectedNodeContentTextBox, "SelectedNodeContentTextBox");
            this.SelectedNodeContentTextBox.Name = "SelectedNodeContentTextBox";
            this.MainToolTip.SetToolTip(this.SelectedNodeContentTextBox, resources.GetString("SelectedNodeContentTextBox.ToolTip"));
            this.SelectedNodeContentTextBox.TextChanged += new System.EventHandler(this.SelectedNodeContentTextBox_TextChanged);
            // 
            // SelectedNodeAttributesGroupBox
            // 
            resources.ApplyResources(this.SelectedNodeAttributesGroupBox, "SelectedNodeAttributesGroupBox");
            this.SelectedNodeAttributesGroupBox.Controls.Add(this.SelectedNodeDetailsTableLayoutPanel);
            this.SelectedNodeAttributesGroupBox.Name = "SelectedNodeAttributesGroupBox";
            this.SelectedNodeAttributesGroupBox.TabStop = false;
            this.MainToolTip.SetToolTip(this.SelectedNodeAttributesGroupBox, resources.GetString("SelectedNodeAttributesGroupBox.ToolTip"));
            // 
            // SelectedNodeDetailsTableLayoutPanel
            // 
            resources.ApplyResources(this.SelectedNodeDetailsTableLayoutPanel, "SelectedNodeDetailsTableLayoutPanel");
            this.SelectedNodeDetailsTableLayoutPanel.Controls.Add(this.SelectedNodeAttributesDataGridView, 0, 0);
            this.SelectedNodeDetailsTableLayoutPanel.Controls.Add(this.AddAttributeButton, 0, 1);
            this.SelectedNodeDetailsTableLayoutPanel.Controls.Add(this.RemoveAttributesButton, 1, 1);
            this.SelectedNodeDetailsTableLayoutPanel.Controls.Add(this.EditAttributeButton, 2, 1);
            this.SelectedNodeDetailsTableLayoutPanel.Name = "SelectedNodeDetailsTableLayoutPanel";
            this.MainToolTip.SetToolTip(this.SelectedNodeDetailsTableLayoutPanel, resources.GetString("SelectedNodeDetailsTableLayoutPanel.ToolTip"));
            // 
            // SelectedNodeAttributesDataGridView
            // 
            resources.ApplyResources(this.SelectedNodeAttributesDataGridView, "SelectedNodeAttributesDataGridView");
            this.SelectedNodeAttributesDataGridView.AllowUserToAddRows = false;
            this.SelectedNodeAttributesDataGridView.AllowUserToDeleteRows = false;
            this.SelectedNodeAttributesDataGridView.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.SelectedNodeAttributesDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.SelectedNodeAttributesDataGridView.AutoGenerateColumns = false;
            this.SelectedNodeAttributesDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.SelectedNodeAttributesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SelectedNodeAttributesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameColumn,
            this.ValueColumn});
            this.SelectedNodeDetailsTableLayoutPanel.SetColumnSpan(this.SelectedNodeAttributesDataGridView, 3);
            this.SelectedNodeAttributesDataGridView.DataSource = this.AttributesBindingSource;
            this.SelectedNodeAttributesDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.SelectedNodeAttributesDataGridView.Name = "SelectedNodeAttributesDataGridView";
            this.SelectedNodeAttributesDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.MainToolTip.SetToolTip(this.SelectedNodeAttributesDataGridView, resources.GetString("SelectedNodeAttributesDataGridView.ToolTip"));
            this.SelectedNodeAttributesDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.SelectedNodeAttributesDataGridView_CellValueChanged);
            this.SelectedNodeAttributesDataGridView.SelectionChanged += new System.EventHandler(this.SelectedNodeAttributesDataGridView_SelectionChanged);
            // 
            // NameColumn
            // 
            this.NameColumn.DataPropertyName = "Name";
            resources.ApplyResources(this.NameColumn, "NameColumn");
            this.NameColumn.Name = "NameColumn";
            // 
            // ValueColumn
            // 
            this.ValueColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ValueColumn.DataPropertyName = "Value";
            resources.ApplyResources(this.ValueColumn, "ValueColumn");
            this.ValueColumn.Name = "ValueColumn";
            // 
            // AttributesBindingSource
            // 
            this.AttributesBindingSource.AllowNew = false;
            // 
            // AddAttributeButton
            // 
            resources.ApplyResources(this.AddAttributeButton, "AddAttributeButton");
            this.AddAttributeButton.Image = global::UserControls.Windows.Forms.Properties.Resources.plus;
            this.AddAttributeButton.Name = "AddAttributeButton";
            this.MainToolTip.SetToolTip(this.AddAttributeButton, resources.GetString("AddAttributeButton.ToolTip"));
            this.AddAttributeButton.UseVisualStyleBackColor = true;
            this.AddAttributeButton.Click += new System.EventHandler(this.AddAttributeButton_Click);
            // 
            // RemoveAttributesButton
            // 
            resources.ApplyResources(this.RemoveAttributesButton, "RemoveAttributesButton");
            this.RemoveAttributesButton.Image = global::UserControls.Windows.Forms.Properties.Resources.times;
            this.RemoveAttributesButton.Name = "RemoveAttributesButton";
            this.MainToolTip.SetToolTip(this.RemoveAttributesButton, resources.GetString("RemoveAttributesButton.ToolTip"));
            this.RemoveAttributesButton.UseVisualStyleBackColor = true;
            this.RemoveAttributesButton.Click += new System.EventHandler(this.RemoveAttributesButton_Click);
            // 
            // EditAttributeButton
            // 
            resources.ApplyResources(this.EditAttributeButton, "EditAttributeButton");
            this.EditAttributeButton.Image = global::UserControls.Windows.Forms.Properties.Resources.edit;
            this.EditAttributeButton.Name = "EditAttributeButton";
            this.MainToolTip.SetToolTip(this.EditAttributeButton, resources.GetString("EditAttributeButton.ToolTip"));
            this.EditAttributeButton.UseVisualStyleBackColor = true;
            this.EditAttributeButton.Click += new System.EventHandler(this.EditAttributeButton_Click);
            // 
            // SaveDocumentDialog
            // 
            this.SaveDocumentDialog.DefaultExt = "xml";
            this.SaveDocumentDialog.FileName = "configuration.xml";
            resources.ApplyResources(this.SaveDocumentDialog, "SaveDocumentDialog");
            // 
            // XmlConfigManagerControl
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MainSplitContainer);
            this.Name = "XmlConfigManagerControl";
            this.MainToolTip.SetToolTip(this, resources.GetString("$this.ToolTip"));
            this.MainSplitContainer.Panel1.ResumeLayout(false);
            this.MainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainSplitContainer)).EndInit();
            this.MainSplitContainer.ResumeLayout(false);
            this.NodesListTableLayoutPanel.ResumeLayout(false);
            this.SelectedNodeDetailsSplitContainer.Panel1.ResumeLayout(false);
            this.SelectedNodeDetailsSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SelectedNodeDetailsSplitContainer)).EndInit();
            this.SelectedNodeDetailsSplitContainer.ResumeLayout(false);
            this.SelectedNodeContentGroupBox.ResumeLayout(false);
            this.SelectedNodeContentGroupBox.PerformLayout();
            this.SelectedNodeAttributesGroupBox.ResumeLayout(false);
            this.SelectedNodeDetailsTableLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SelectedNodeAttributesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AttributesBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer MainSplitContainer;
        private System.Windows.Forms.TableLayoutPanel NodesListTableLayoutPanel;
        private System.Windows.Forms.TreeView NodesTreeView;
        private System.Windows.Forms.Button AddNodeButton;
        private System.Windows.Forms.Button RemoveSelectedNodeButton;
        private System.Windows.Forms.ToolTip MainToolTip;
        private System.Windows.Forms.Button EditSelectedNodeButton;
        private System.Windows.Forms.SplitContainer SelectedNodeDetailsSplitContainer;
        private System.Windows.Forms.GroupBox SelectedNodeContentGroupBox;
        private System.Windows.Forms.TextBox SelectedNodeContentTextBox;
        private System.Windows.Forms.GroupBox SelectedNodeAttributesGroupBox;
        private System.Windows.Forms.TableLayoutPanel SelectedNodeDetailsTableLayoutPanel;
        private System.Windows.Forms.DataGridView SelectedNodeAttributesDataGridView;
        private System.Windows.Forms.Button AddAttributeButton;
        private System.Windows.Forms.Button RemoveAttributesButton;
        private System.Windows.Forms.Button EditAttributeButton;
        private System.Windows.Forms.BindingSource AttributesBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValueColumn;
        private System.Windows.Forms.SaveFileDialog SaveDocumentDialog;
    }
}
