﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing.Design;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using XMLConfig;

namespace UserControls.Windows.Forms
{
    public partial class XmlConfigManagerControl : UserControl
    {
        #region Constructors

        /// <summary>
        /// Constructor.
        /// </summary>
        public XmlConfigManagerControl()
        {
            InitializeComponent();

            ConfigDocument = new XConfigDocument();
            RefreshControls();
        }

        #endregion Constructors


        #region Methods

        /// <summary>
        /// Populates the given nodes collection
        /// </summary>
        /// <param name="nodeCollection"></param>
        /// <param name="xmlElement"></param>
        private void AddElementToNodesCollection(TreeNodeCollection nodeCollection, XElement xmlElement)
        {
            if (xmlElement.NodeType == System.Xml.XmlNodeType.Element)
            {
                var newNode = nodeCollection.Add(xmlElement.Name.LocalName);

                foreach (var element in xmlElement.Elements())
                {
                    AddElementToNodesCollection(newNode.Nodes, element);
                }
            }
        }

        /// <summary>
        /// Refreshes the state of the inner controls.
        /// </summary>
        private void RefreshControls()
        {
            MainSplitContainer.Panel2.Enabled =
            AddNodeButton.Enabled = NodesTreeView.SelectedNode != null;
            EditSelectedNodeButton.Enabled =
            RemoveSelectedNodeButton.Enabled =
            SelectedNodeDetailsSplitContainer.Enabled = NodesTreeView.SelectedNode != null && NodesTreeView.SelectedNode != NodesTreeView.Nodes[0];
            RemoveAttributesButton.Enabled = SelectedNodeAttributesDataGridView.SelectedRows.Count > 0;
            EditAttributeButton.Enabled = SelectedNodeAttributesDataGridView.SelectedCells.Count == 1;
        }

        /// <summary>
        /// Refresh the controls of the details panel.
        /// </summary>
        private void RefreshDetails()
        {
            var selectedNode = NodesTreeView.SelectedNode;

            // Clear controls.
            SelectedNodeContentTextBox.Clear();
            AttributesBindingSource.Clear();

            // If node selected, assign controls values.
            if (NodesTreeView.SelectedNode != null)
            {
                var selectedElement = GetElementFromNode(selectedNode);

                SelectedNodeContentTextBox.Text = (selectedElement.FirstNode as XText)?.Value;
                AttributesBindingSource.DataSource = selectedElement.Attributes().Select(attr => new { Name = attr.Name.LocalName, attr.Value }).ToList();
            }
        }

        /// <summary>
        /// Gets the XML element corresponding to the specified node.
        /// </summary>
        /// <param name="node">The <see cref="NodesTreeView"/> node.</param>
        /// <returns>The XML element matching the specified node.</returns>
        private XElement GetElementFromNode(TreeNode node)
        {
            var path = '/' + node.FullPath.Replace('\\', '/');
            var index = 0;

            if (node.Parent != null)
            {
                var nodes = node.Parent.Nodes.OfType<TreeNode>().Where(n => n.Text == node.Text).ToArray();

                index = Array.IndexOf(nodes, node);
            }

            return ConfigDocument.XPathSelectElement($"{path}[{index + 1}]");
        }

        /// <summary>
        /// Saves the configuration document on disk with the filename selected by the user.
        /// </summary>
        public void SaveAs()
        {
            if (!string.IsNullOrWhiteSpace(ConfigurationFile))
            {
                SaveDocumentDialog.FileName = System.IO.Path.GetFileName(ConfigurationFile);
            }

            if (SaveDocumentDialog.ShowDialog(this) == DialogResult.OK)
            {
                _configFilePath = SaveDocumentDialog.FileName;

                ConfigDocument.Save(ConfigurationFile);
            }
        }
        
        /// <summary>
        /// Saves the configuration document on disk.
        /// </summary>
        public void Save()
        {
            if (string.IsNullOrWhiteSpace(ConfigurationFile))
            {
                SaveAs();
                return;
            }

            ConfigDocument.Save(ConfigurationFile);
        }

        #region Event handlers

        /// <summary>
        /// Refresehs controls after node selection has changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NodesTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            RefreshDetails();
            RefreshControls();
        }

        /// <summary>
        /// Avoids changing the name of the root element.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NodesTreeView_BeforeLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            e.CancelEdit = e.Node == NodesTreeView.Nodes[0];
        }

        /// <summary>
        /// Changes the corresponding document element's name when a node's text is changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NodesTreeView_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(e.Label) || e.Label == e.Node.Text)
            {
                e.CancelEdit = true;

                return;
            }

            try
            {
                GetElementFromNode(e.Node).Name = e.Label;
            }
            catch (XmlException ex)
            {
                e.CancelEdit = true;

                MessageBox.Show(this, ex.Message, XmlConfigManagerControl_Messages.XmlErrorOccurred, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Adds a new element to the document under the currently selected one.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddNodeButton_Click(object sender, EventArgs e)
        {
            var element = GetElementFromNode(NodesTreeView.SelectedNode);
            var newElement = new XElement(XmlConfigManagerControl_Messages.NewElementName);

            element.Add(newElement);
            AddElementToNodesCollection(NodesTreeView.SelectedNode.Nodes, newElement);

            NodesTreeView.SelectedNode = NodesTreeView.SelectedNode.LastNode;
            NodesTreeView.Focus();

            RefreshControls();
        }

        /// <summary>
        /// Removes the selected element and all it's children.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RemoveNodeButton_Click(object sender, EventArgs e)
        {
            var node = NodesTreeView.SelectedNode;
            var element = GetElementFromNode(node);

            element.Remove();
            node.Remove();
        }

        /// <summary>
        /// Begins the edition of the selected node.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditSelectedNodeButton_Click(object sender, EventArgs e)
        {
            NodesTreeView.SelectedNode?.BeginEdit();
        }

        /// <summary>
        /// Changes the text content of the selected XML element when the <see cref="SelectedNodeContentTextBox"/> text changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedNodeContentTextBox_TextChanged(object sender, EventArgs e)
        {
            if (SelectedNodeContentTextBox.Focused)
            {
                var element = GetElementFromNode(NodesTreeView.SelectedNode);

                (element.FirstNode as XText)?.Remove();

                var textNode = new XText(SelectedNodeContentTextBox.Text);

                if (!string.IsNullOrWhiteSpace(textNode.Value))
                {
                    element.AddFirst(textNode);
                }
            }
        }

        /// <summary>
        /// Add's a new attribute to the selected element.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddAttributeButton_Click(object sender, EventArgs e)
        {
            var element = GetElementFromNode(NodesTreeView.SelectedNode);
            var index = 1;
            var attributeName = $"{XmlConfigManagerControl_Messages.NewAttributeName}-{index}";

            while (element.Attributes(attributeName).Count() > 0)
            {
                ++index;

                attributeName = $"{XmlConfigManagerControl_Messages.NewAttributeName}-{index}";
            }

            element.Add(new XAttribute(attributeName, string.Empty));
            RefreshDetails();
        }

        /// <summary>
        /// Removes the selected attributes from the selected element.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RemoveAttributesButton_Click(object sender, EventArgs e)
        {
            var element = GetElementFromNode(NodesTreeView.SelectedNode);

            foreach (DataGridViewRow row in SelectedNodeAttributesDataGridView.SelectedRows)
            {
                element.Attributes().First(attr => attr.Name.LocalName == ((dynamic)row.DataBoundItem).Name).Remove();
                SelectedNodeAttributesDataGridView.Rows.Remove(row);
            }
        }

        /// <summary>
        /// Begins editing the selected cell in the <see cref="SelectedNodeAttributesDataGridView"/> control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditAttributeButton_Click(object sender, EventArgs e)
        {
            SelectedNodeAttributesDataGridView.CurrentCell.ReadOnly = false;
            SelectedNodeAttributesDataGridView.BeginEdit(true);
        }

        /// <summary>
        /// Refreshes the controls when the <see cref="SelectedNodeAttributesDataGridView"/> selection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedNodeAttributesDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            RefreshControls();
        }

        private void SelectedNodeAttributesDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (NodesTreeView.SelectedNode != null)
            {
                var cell = SelectedNodeAttributesDataGridView.CurrentCell;
                var element = GetElementFromNode(NodesTreeView.SelectedNode);
                dynamic boundItem = SelectedNodeAttributesDataGridView.Rows[e.RowIndex].DataBoundItem;
                XAttribute attribute = element.Attribute(boundItem.Name);

                try
                {
                    if (e.ColumnIndex == 0)
                    {
                        attribute.Remove();
                        element.SetAttributeValue(cell.EditedFormattedValue as string, boundItem.Value);
                    }
                    else
                    {
                        element.SetAttributeValue(boundItem.Name, cell.EditedFormattedValue);
                    }
                }
                catch(XmlException ex)
                {
                    MessageBox.Show(this, ex.Message, XmlConfigManagerControl_Messages.XmlErrorOccurred, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    RefreshDetails();
                }
            }
        }

        #endregion Event handlers

        #endregion Methods


        #region Properties

        /// <summary>
        /// XML Configuration document.
        /// </summary>
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Always), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public XConfigDocument ConfigDocument
        {
            get => _configDocument;
            set
            {
                _configDocument = value;
                OriginalDocumentHash = value.DocumentHash;

                NodesTreeView.Nodes.Clear();
                AddElementToNodesCollection(NodesTreeView.Nodes, _configDocument.Root);
                NodesTreeView.ExpandAll();
                NodesTreeView.SelectedNode = NodesTreeView.Nodes[0];
                RefreshControls();
                NodesTreeView.Focus();
            }
        }

        [Browsable(true), EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        [Category("XML Configuration Document"), Description("The XML configuration document file path.")]
        [Editor(typeof(System.Windows.Forms.Design.FileNameEditor), typeof(UITypeEditor))]
        public string ConfigurationFile
        {
            get => _configFilePath;
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    _configFilePath = null;

                    return;
                }

                try
                {
                    ConfigDocument = new XConfigDocument(XDocument.Load(value));
                    _configFilePath = value;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, ex.Message, XmlConfigManagerControl_Messages.ErrorLoadingDocument, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Gets the SHA1 hash of the original document.
        /// </summary>
        [Category("XML Configuration Document"), Description("The original XML configuration document SHA1 hash.")]
        public string OriginalDocumentHash { get; private set; }

        /// <summary>
        /// Gets if the XML document has been changed (<code>true</code>) or not (<code>false</code>).
        /// </summary>
        [Category("XML Configuration Document"), Description("Tells if the document has been changed.")]
        public bool DocumentHasChanged
        {
            get => ConfigDocument.DocumentHash != OriginalDocumentHash;
        }

        #endregion Properties

        #region Variables

        private string _configFilePath;
        private XConfigDocument _configDocument;

        #endregion Variables
    }
}
